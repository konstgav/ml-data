# DVC-репозиторий с тестовым датасетом для машинного обучения

Технология [dvc](https://dvc.org/) организует версионирование датасета для обучения моделей, позволяет переключаться между коммитами. Работает поверх git, хранит хэш-суммы файлов с данными в git-репозитории, сами данные хранятся в hdfs.

## Скрипт для создания dvc-репозитория

```(bash)
mkdir ml-data
cd ml-data
git init
dvc init
git commit -m "Initialize DVC"
cp ../winequality-red.csv .
dvc add winequality-red.csv
git add .gitignore winequality-red.csv.dvc
git commit -m "Add csv"
dvc remote add -d storage webhdfs://root@localhost:50070/user/root/input/storage
git add .dvc/config
git commit -m "Configure remote storage"
dvc push
```

## Скрипт для загрузки данных (выполняется в jupyter-ноутбуке ml-инженером)

```(bash)
git clone https://gitlab.com/konstgav/ml-data.git
dvc pull
```
